# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import warnings

from flask_wtf import FlaskForm as Form
from jinja2 import TemplateNotFound
from werkzeug.exceptions import abort
from werkzeug.utils import redirect
from wtforms import (
    IntegerField, SelectField, StringField, ValidationError, validators)

from trytond.i18n import gettext
from trytond.model import fields
from trytond.modules.party.exceptions import InvalidPhoneNumber
from trytond.pool import Pool, PoolMeta

from nereid import (
    current_user, current_website, flash, jsonify, login_required,
    render_template, request, route, url_for)
from nereid.contrib.locale import make_lazy_gettext

from .user import RegistrationForm

_ = make_lazy_gettext('nereid_base')


class AddressForm(Form):
    "Address Form"
    name = StringField(_('Name'), [validators.DataRequired(), ])
    street = StringField(_('Street'), [validators.DataRequired(), ])
    postal_code = StringField(_('Post Code'), [validators.DataRequired(), ])
    city = StringField(_('City'), [validators.DataRequired(), ])
    country = SelectField(_('Country'), [validators.DataRequired(), ], coerce=int)  # noqa
    subdivision = IntegerField(_('State/County'), [validators.DataRequired()])
    email = StringField(_('Email'))
    phone = StringField(_('Phone'))

    def __init__(self, formdata=None, **kwargs):
        super(AddressForm, self).__init__(formdata, **kwargs)

        # Fill country choices while form is initialized
        self.country.choices = [
            (c.id, c.name) for c in current_website.countries
        ]

    def validate_phone(form, field):
        pool = Pool()
        ContactMechanism = pool.get('party.contact_mechanism')

        # Skip validation for guest users without known address
        if not field.data or not getattr(current_user, 'party', None):
            return
        mechanism = ContactMechanism()
        mechanism.party = current_user.party
        mechanism.type = 'phone'
        mechanism.value = field.data

        try:
            ContactMechanism.check_valid_phonenumber([mechanism])
        except InvalidPhoneNumber:
            msg = gettext('nereid_base.msg_invalid_phone_number',
                    phone=field.data)
            raise ValidationError(msg)


class Address(metaclass=PoolMeta):
    __name__ = 'party.address'

    registration_form = RegistrationForm

    @classmethod
    def get_address_form(cls, address=None):
        """
        Return an initialised Address form that can be validated and used to
        create/update addresses

        :param address: If an active record is provided it is used to autofill
                        the form.
        """
        if address:
            email = address.party.contact_mechanism_get(
                types='email', usage='web')
            phone = address.party.contact_mechanism_get(
                types=['phone', 'mobile'], usage='web')
            form = AddressForm(
                request.form,
                name=address.party_name,
                street=address.street,
                postal_code=address.postal_code,
                city=address.city,
                country=address.country and address.country.id,
                subdivision=address.subdivision and address.subdivision.id,
                email=email.value if email else None,
                phone=phone.value if phone else None,
                )
        else:
            address_name = "" if current_user.is_anonymous else \
                current_user.name
            form = AddressForm(request.form, name=address_name)

        return form

    @classmethod
    @route("/create-address", methods=["GET", "POST"])
    @login_required
    def create_address(cls):
        """
        Create an address for the current nereid_user

        GET
        ~~~

        Return an address creation form

        POST
        ~~~~

        Creates an address and redirects to the address view. If a next_url
        is provided, redirects there.

        .. version_added: 3.0.3.0
        """
        form = cls.get_address_form()

        if request.method == 'POST' and form.validate():
            party = current_user.party
            address, = cls.create([{
                'party_name': form.name.data,
                'street': form.street.data,
                'postal_code': form.postal_code.data,
                'city': form.city.data,
                'country': form.country.data,
                'subdivision': form.subdivision.data,
                'party': party.id,
            }])
            if form.email.data:
                party.add_contact_mechanism_if_not_exists(
                    'email', form.email.data, 'web')
            if form.phone.data:
                party.add_contact_mechanism_if_not_exists(
                    'phone', form.phone.data, 'web')
            return redirect(url_for('party.address.view_address'))

        try:
            return render_template('address-add.jinja', form=form)
        except TemplateNotFound:
            # The address-add template was introduced in 3.0.3.0
            # so just raise a deprecation warning till 3.2.X and then
            # expect the use of address-add template
            warnings.warn(
                "address-add.jinja template not found. "
                "Will be required in future versions",
                DeprecationWarning
            )
            return render_template('address-edit.jinja', form=form)

    @classmethod
    @route("/save-new-address", methods=["GET", "POST"])
    @route("/edit-address/<int:address>", methods=["GET", "POST"])
    @login_required
    def edit_address(cls, address=None):
        """
        Edit an Address

        POST will update an existing address.
        GET will return a existing address edit form.

        .. version_changed:: 3.0.3.0

            For creating new address use the create_address handled instead of
            this one. The functionality would be deprecated in 3.2.X

        :param address: ID of the address
        """
        if address is None:
            warnings.warn(
                "Address creation will be deprecated from edit_address handler."
                " Use party.address.create_address instead",
                DeprecationWarning
            )
            return cls.create_address()

        form = cls.get_address_form()

        if address not in (a.id for a in current_user.party.addresses):
            # Check if the address is in the list of addresses of the
            # current user's party
            abort(403)

        address = cls(address)

        if request.method == 'POST' and form.validate():
            party = current_user.party
            cls.write([address], {
                'party_name': form.name.data,
                'street': form.street.data,
                'postal_code': form.postal_code.data,
                'city': form.city.data,
                'country': form.country.data,
                'subdivision': form.subdivision.data,
            })
            if form.email.data:
                party.add_contact_mechanism_if_not_exists(
                    'email', form.email.data, 'web'
                )
            if form.phone.data:
                party.add_contact_mechanism_if_not_exists(
                    'phone', form.phone.data, 'web'
                )
            return redirect(url_for('party.address.view_address'))

        elif request.method == 'GET' and address:
            # Its an edit of existing address, prefill data
            form = cls.get_address_form(address)

        return render_template(
            'address-edit.jinja', form=form, address=address)

    @classmethod
    @route("/view-address", methods=["GET"])
    @login_required
    def view_address(cls):
        "View the addresses of user"
        return render_template('address.jinja')

    @route("/remove-address/<int:active_id>", methods=["POST"])
    @login_required
    def remove_address(self):
        """
        Make address inactive if user removes the address from address book.
        """
        if self.party == current_user.party:
            self.active = False
            self.save()
            flash(_('Address has been deleted successfully!'))
            if request.is_xhr:
                return jsonify(success=True)
            return redirect(request.referrer)

        abort(403)


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    nereid_users = fields.One2Many('nereid.user', 'party', 'Web Users')

    def add_contact_mechanism_if_not_exists(self, type, value, usage=None):
        """
        Adds a contact mechanism to the party if it does not exist

        :return: The created contact mechanism or the one which existed
        """
        ContactMechanism = Pool().get('party.contact_mechanism')

        mechanism = self.contact_mechanism_get(types=type, usage=usage)
        if mechanism is None:
            mechanism = ContactMechanism()
            mechanism.party = self.id
            mechanism.type = type
        mechanism.value = value
        if usage is not None:
            setattr(mechanism, usage, True)
        mechanism.save()
        return mechanism

    @classmethod
    def copy(cls, parties, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['nereid_users'] = None
        return super(Party, cls).copy(parties, default=default)


class ContactMechanismForm(Form):
    "Contact Mechanism Form"
    type = SelectField('Type', [validators.DataRequired()])
    value = StringField('Value', [validators.DataRequired()])
    comment = StringField('Comment')


class ContactMechanism(metaclass=PoolMeta):
    __name__ = "party.contact_mechanism"

    web = fields.Boolean('Web')

    @classmethod
    def usages(cls, _fields=None):
        if _fields is None:
            _fields = []
        _fields.append('web')
        return super().usages(_fields=_fields)

    @classmethod
    def get_form(cls):
        """
        Returns the contact mechanism form
        """
        from trytond.modules.party import contact_mechanism
        form = ContactMechanismForm()
        form.type.choices = contact_mechanism._TYPES
        return form

    @classmethod
    @route("/contact-mechanisms/add", methods=["POST"])
    @login_required
    def add(cls):
        """
        Adds a contact mechanism to the party's contact mechanisms
        """
        form = cls.get_form()
        if form.validate_on_submit():
            cls.create([{
                'party': current_user.party.id,
                'type': form.type.data,
                'value': form.value.data,
                'comment': form.comment.data,
            }])
            if request.is_xhr:
                return jsonify({'success': True})
            return redirect(request.referrer)

        if request.is_xhr:
            return jsonify({'success': False})
        else:
            for field, messages in form.errors:
                flash("<br>".join(messages), "Field %s" % field)
            return redirect(request.referrer)

    @route("/contact-mechanisms/<int:active_id>", methods=["POST", "DELETE"])
    @login_required
    def remove(self):
        """
        DELETE: Removes the current contact mechanism
        """
        ContactMechanism = Pool().get('party.contact_mechanism')

        if self.party == current_user.party:
            ContactMechanism.delete([self])
        else:
            abort(403)
        if request.is_xhr:
            return jsonify({
                'success': True
            })
        return redirect(request.referrer)


class PartyErase(metaclass=PoolMeta):
    __name__ = 'party.erase'

    def to_erase(self, party_id):
        pool = Pool()
        User = pool.get('nereid.user')
        to_erase = super(PartyErase, self).to_erase(party_id)
        to_erase.append(
            (User, [('party', '=', party_id)], True,
                ['name', 'email'],
                [None, None]))
        return to_erase


class PartyReplace(metaclass=PoolMeta):
    __name__ = 'party.replace'

    @classmethod
    def fields_to_replace(cls):
        return super(PartyReplace, cls).fields_to_replace() + [
            ('nereid.user', 'party'),
            ]
