[![Tests Status](https://m9s.gitlab.io/nereid_base/junit/junit-badge.svg)](https://m9s.gitlab.io/nereid_base)

[![Coverage Status](https://m9s.gitlab.io/nereid_base/coverage/coverage-badge.svg)](https://m9s.gitlab.io/nereid_base)

This module runs with the Tryton application platform.

Installing
----------

See INSTALL

Note
----

This module is developed and tested over a patched Tryton server and
core modules. Maybe some of these patches are required for the module to work.

Support
-------

For more information or if you encounter any problems with this module,
please contact the programmers at

#### MBSolutions

   * Issues:   https://gitlab.com/m9s/nereid_base/issues
   * Website:  http://www.m9s.biz/
   * Email:    info@m9s.biz

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, forum or IRC channel:

   * http://bugs.tryton.org/
   * http://www.tryton.org/forum
   * irc://irc.freenode.net/tryton

License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

